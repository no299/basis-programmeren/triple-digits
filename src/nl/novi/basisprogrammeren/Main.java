package nl.novi.basisprogrammeren;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        while(true){
            Random randomizer = new Random();

            int maximum = 9;

            int firstDigit = randomizer.nextInt(maximum) + 1;
            int secondDigit = randomizer.nextInt(maximum) + 1;
            int thirdDigit = randomizer.nextInt(maximum) + 1;

            int sum = firstDigit + secondDigit + thirdDigit;
            int product = firstDigit * secondDigit * thirdDigit;

            System.out.println("Som: " + sum);
            System.out.println("Product: " + product);

            Scanner userInput = new Scanner(System.in);
            int maxRounds = 3;

            for(int currentRound = 1; currentRound <= maxRounds; currentRound++){
                System.out.println("Voer drie getallen in, ronde " + currentRound + " van " + maxRounds);

                int userChoice1 = userInput.nextInt();
                int userChoice2 = userInput.nextInt();
                int userChoice3 = userInput.nextInt();

                System.out.println("Je hebt de volgende getallen ingevoerd");
                System.out.println("Getal 1: " + userChoice1);
                System.out.println("Getal 2: " + userChoice2);
                System.out.println("Getal 3: " + userChoice3);

                int userChoiceSum = userChoice1 + userChoice2 + userChoice3;
                int userChoiceProduct = userChoice1 * userChoice2 * userChoice3;

                boolean playerHasWon = userChoiceSum == sum && userChoiceProduct == product;

                if(playerHasWon){
                    System.out.println("Gefeliciteerd. je hebt gewonnen");
                    break;
                } else {
                    System.out.println("Helaas, je hebt verloren");
                }
            }

            boolean continuePlaying = false;

            System.out.println("Wil je het spel nogmaals spelen? Toets y/n");

            String continuePlayingUserChoice = userInput.next();

            switch(continuePlayingUserChoice) {
                case "y":
                    System.out.println("Je hebt ervoor gekozen nogmaals te spelen");
                    continuePlaying = true;
                    break;
                case "n":
                    System.out.println("Je hebt ervoor gekozen te stoppen ");
                    break;
                default:
                    System.out.println("Je hebt geen geldige waarde ingevoerd");
                    break;
            }

            if(!continuePlaying){  // !continuePlaying is hetzelfde als continuePlaying == false
                break;
            }
        }

        System.out.println("Het spel is afgelopen");
    }
}
